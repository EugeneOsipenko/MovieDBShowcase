package com.eugeneosipenko.moviedbshowcase

import android.arch.paging.PagedList
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import com.eugeneosipenko.moviedbshowcase.adapter.MainThreadExecutor
import com.eugeneosipenko.moviedbshowcase.adapter.TVShowSimilarAdapter
import com.eugeneosipenko.moviedbshowcase.api.TVShowAPI
import com.eugeneosipenko.moviedbshowcase.datasource.TVShowDataSource
import com.eugeneosipenko.moviedbshowcase.model.TVShowModel
import kotlinx.android.synthetic.main.activity_tv_show_details.*
import java.util.concurrent.Executors

class TVShowDetailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tv_show_details)

        initUI()
        initSimilarShows()
    }

    private fun initUI() {
        val model = intent.getParcelableExtra<TVShowModel>(TVShowModel::javaClass.name)
                ?: throw NullPointerException("Model cannot be null")

        name.text = model.name
        date.text = DateFormatter.format(model)
        rating.text = getString(R.string.tv_show_rating, model.voteAverage)
        popularity.text = getString(R.string.tv_show_popularity, model.popularity)
        votes.text = getString(R.string.tv_show_votes, model.voteCount)
        overview.text = model.overview

        GlideApp.with(this)
                .load("${TVShowAPI.BASE_IMAGE_URL}${model.posterPath}")
                .into(poster)

        GlideApp.with(this)
                .load("${TVShowAPI.BACKDROP_IMAGE_URL}${model.backdropPath}")
                .into(backdrop)
    }

    private fun initSimilarShows() {
        val tvShowApi = TVShowAPI.API
        val pageSize = 20
        val config = PagedList.Config.Builder()
                .setPageSize(pageSize)
                .setEnablePlaceholders(false)
                .setInitialLoadSizeHint(pageSize * 2)
                .build()

        val pagedList = PagedList.Builder<Int, TVShowModel>(TVShowDataSource(tvShowApi), config)
                .setNotifyExecutor(MainThreadExecutor())
                .setFetchExecutor(Executors.newSingleThreadExecutor())
                .build()

        val adapter = TVShowSimilarAdapter(this::launchTVShowDetailsActivity)
        adapter.submitList(pagedList)

        similarShowsList.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        similarShowsList.adapter = adapter
    }

    private fun launchTVShowDetailsActivity(model: TVShowModel) {
        startActivity(Intent(this, TVShowDetailActivity::class.java).apply {
            putExtra(TVShowModel::javaClass.name, model)
        })
    }
}