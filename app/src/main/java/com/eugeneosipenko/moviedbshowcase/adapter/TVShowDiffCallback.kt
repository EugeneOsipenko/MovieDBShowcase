package com.eugeneosipenko.moviedbshowcase.adapter

import android.support.v7.util.DiffUtil
import com.eugeneosipenko.moviedbshowcase.model.TVShowModel

class TVShowDiffCallback : DiffUtil.ItemCallback<TVShowModel>() {
    override fun areItemsTheSame(oldItem: TVShowModel, newItem: TVShowModel) = oldItem.id == newItem.id
    override fun areContentsTheSame(oldItem: TVShowModel, newItem: TVShowModel) = oldItem == newItem
}