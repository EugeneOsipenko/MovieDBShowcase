package com.eugeneosipenko.moviedbshowcase.adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.eugeneosipenko.moviedbshowcase.model.TVShowModel
import kotlinx.android.synthetic.main.item_tv_show.view.*

class TVShowViewHolder(
        private val itemClickListener: (TVShowModel) -> Unit,
        view: View
) : RecyclerView.ViewHolder(view) {
    val poster: ImageView = view.poster
    val title: TextView = view.title
    val date: TextView = view.date
    val rating: TextView = view.rating
    val popularity: TextView = view.popularity
    val votes: TextView = view.votes
    var model: TVShowModel? = null

    init {
        view.setOnClickListener { model?.let { itemClickListener.invoke(it) } }
    }
}