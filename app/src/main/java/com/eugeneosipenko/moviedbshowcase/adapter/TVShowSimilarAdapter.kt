package com.eugeneosipenko.moviedbshowcase.adapter

import android.arch.paging.PagedListAdapter
import android.view.LayoutInflater
import android.view.ViewGroup
import com.eugeneosipenko.moviedbshowcase.DateFormatter
import com.eugeneosipenko.moviedbshowcase.GlideApp
import com.eugeneosipenko.moviedbshowcase.R
import com.eugeneosipenko.moviedbshowcase.model.TVShowModel
import com.eugeneosipenko.moviedbshowcase.api.TVShowAPI

class TVShowSimilarAdapter(
        private val itemClickListener: (TVShowModel) -> Unit
) : PagedListAdapter<TVShowModel, TVShowSimilarViewHolder>(TVShowDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TVShowSimilarViewHolder {
        return TVShowSimilarViewHolder(
                itemClickListener,
                LayoutInflater.from(parent.context).inflate(R.layout.item_similar_tv_show, parent, false))
    }

    override fun onBindViewHolder(holder: TVShowSimilarViewHolder, position: Int) {
        val model = getItem(position)!!

        holder.model = model
        holder.title.text = model.name
        holder.date.text = DateFormatter.format(model)

        GlideApp.with(holder.poster)
                .load("${TVShowAPI.BASE_IMAGE_URL}${model.posterPath}")
                .into(holder.poster)
    }
}