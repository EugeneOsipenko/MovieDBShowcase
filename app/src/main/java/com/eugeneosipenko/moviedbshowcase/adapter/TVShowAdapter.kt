package com.eugeneosipenko.moviedbshowcase.adapter

import android.arch.paging.PagedListAdapter
import android.view.LayoutInflater
import android.view.ViewGroup
import com.eugeneosipenko.moviedbshowcase.DateFormatter
import com.eugeneosipenko.moviedbshowcase.GlideApp
import com.eugeneosipenko.moviedbshowcase.R
import com.eugeneosipenko.moviedbshowcase.model.TVShowModel
import com.eugeneosipenko.moviedbshowcase.api.TVShowAPI

class TVShowAdapter(
        private val itemClickListener: (TVShowModel) -> Unit
) : PagedListAdapter<TVShowModel, TVShowViewHolder>(TVShowDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TVShowViewHolder {
        return TVShowViewHolder(
                itemClickListener,
                LayoutInflater.from(parent.context).inflate(R.layout.item_tv_show, parent, false))
    }

    override fun onBindViewHolder(holder: TVShowViewHolder, position: Int) {
        val resources = holder.itemView.resources
        val model = getItem(position)!!

        holder.model = model
        holder.title.text = model.name
        holder.date.text = DateFormatter.format(model)
        holder.rating.text = resources.getString(R.string.tv_show_rating, model.voteAverage)
        holder.popularity.text = resources.getString(R.string.tv_show_popularity, model.popularity)
        holder.votes.text = resources.getString(R.string.tv_show_votes, model.voteCount)

        GlideApp.with(holder.poster)
                .load("${TVShowAPI.BASE_IMAGE_URL}${model.posterPath}")
                .into(holder.poster)
    }
}