package com.eugeneosipenko.moviedbshowcase.datasource

import android.arch.paging.PageKeyedDataSource
import com.eugeneosipenko.moviedbshowcase.model.TVShowModel
import com.eugeneosipenko.moviedbshowcase.api.TVShowAPI
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class TVShowDataSource(
        private val tvShowApi: TVShowAPI
) : PageKeyedDataSource<Int, TVShowModel>() {

    override fun loadInitial(params: LoadInitialParams<Int>, callback: LoadInitialCallback<Int, TVShowModel>) {
        tvShowApi.getPopular()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { response, _ ->
                    if (response != null) {
                        callback.onResult(response.results, 0, response.totalResults, null, response.page + 1)
                    }
                }
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, TVShowModel>) {
        tvShowApi.getPopular(params.key)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { response, _ ->
                    if (response != null) {
                        callback.onResult(response.results, response.page + 1)
                    }
                }
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, TVShowModel>) {
        // no op
    }
}