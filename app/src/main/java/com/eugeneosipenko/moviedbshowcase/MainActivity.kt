package com.eugeneosipenko.moviedbshowcase

import android.arch.paging.PagedList
import android.content.Intent
import android.graphics.Rect
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import com.eugeneosipenko.moviedbshowcase.adapter.MainThreadExecutor
import com.eugeneosipenko.moviedbshowcase.adapter.TVShowAdapter
import com.eugeneosipenko.moviedbshowcase.api.TVShowAPI
import com.eugeneosipenko.moviedbshowcase.datasource.TVShowDataSource
import com.eugeneosipenko.moviedbshowcase.model.TVShowModel
import kotlinx.android.synthetic.main.activity_main.*
import java.util.concurrent.Executors

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initAdapter()
    }

    private fun initAdapter() {
        val itemBottomMargin = resources.getDimensionPixelSize(R.dimen.item_bottom_margin)
        val tvShowApi = TVShowAPI.API
        val pageSize = 20
        val config = PagedList.Config.Builder()
                .setPageSize(pageSize)
                .setEnablePlaceholders(false)
                .setInitialLoadSizeHint(pageSize * 2)
                .build()

        val pagedList = PagedList.Builder<Int, TVShowModel>(TVShowDataSource(tvShowApi), config)
                .setNotifyExecutor(MainThreadExecutor())
                .setFetchExecutor(Executors.newSingleThreadExecutor())
                .build()

        val adapter = TVShowAdapter(this::launchTVShowDetailsActivity)
        adapter.submitList(pagedList)

        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = adapter
        recyclerView.addItemDecoration(object : RecyclerView.ItemDecoration() {
            override fun getItemOffsets(outRect: Rect, view: View?, parent: RecyclerView?, state: RecyclerView.State?) {
                outRect.set(0, 0, 0, itemBottomMargin)
            }
        })
    }

    private fun launchTVShowDetailsActivity(model: TVShowModel) {
        startActivity(Intent(this, TVShowDetailActivity::class.java).apply {
            putExtra(TVShowModel::javaClass.name, model)
        })
    }
}
