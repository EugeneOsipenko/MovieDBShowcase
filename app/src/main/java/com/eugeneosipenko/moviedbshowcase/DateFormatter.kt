package com.eugeneosipenko.moviedbshowcase

import com.eugeneosipenko.moviedbshowcase.model.TVShowModel
import java.text.SimpleDateFormat
import java.util.*

object DateFormatter {

    private val appFormat = SimpleDateFormat("MMM yyyy", Locale.US)
    private val modelFormat = SimpleDateFormat("yyyy-MM-dd", Locale.US)

    fun format(model: TVShowModel): String {
        val date = modelFormat.parse(model.date)
        return appFormat.format(date)
    }
}