package com.eugeneosipenko.moviedbshowcase.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class TVShowModel(
        @SerializedName("id") val id: Int,
        @SerializedName("name") val name: String,
        @SerializedName("first_air_date") val date: String,
        @SerializedName("overview") val overview: String,
        @SerializedName("backdrop_path") val backdropPath: String,
        @SerializedName("poster_path") val posterPath: String,
        @SerializedName("popularity") val popularity: Float,
        @SerializedName("vote_average") val voteAverage: Float,
        @SerializedName("vote_count") val voteCount: Int
) : Parcelable