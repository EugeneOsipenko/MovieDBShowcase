package com.eugeneosipenko.moviedbshowcase.api

import com.eugeneosipenko.moviedbshowcase.model.TVShowModel
import com.google.gson.annotations.SerializedName

data class TVShowResponse(
        @SerializedName("page") val page: Int,
        @SerializedName("results") val results: List<TVShowModel>,
        @SerializedName("total_results") val totalResults: Int
)