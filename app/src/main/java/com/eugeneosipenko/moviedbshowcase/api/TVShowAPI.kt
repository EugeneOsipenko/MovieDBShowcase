package com.eugeneosipenko.moviedbshowcase.api

import io.reactivex.Single
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface TVShowAPI {

    @GET("tv/popular")
    fun getPopular(
            @Query("page") page: Int? = null
    ): Single<TVShowResponse>

    @GET("tv/{tv_id}/similar")
    fun getSimilar(
            @Path("tv_id") id: Int,
            @Query("page") page: Int? = null
    ): Single<TVShowResponse>

    companion object {

        const val BASE_IMAGE_URL = "https://image.tmdb.org/t/p/w342"
        const val BACKDROP_IMAGE_URL = "https://image.tmdb.org/t/p/w780"

        val API: TVShowAPI by lazy { create() }

        private fun create(): TVShowAPI {
            val client = OkHttpClient.Builder().addInterceptor(AuthInterceptor()).build()
            return Retrofit.Builder()
                    .baseUrl("https://api.themoviedb.org/3/")
                    .client(client)
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
                    .create(TVShowAPI::class.java)
        }
    }
}
